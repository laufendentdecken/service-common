package at.running.platform.common;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import at.running.platform.common.model.Help;

public class Command {

	public Command() {
		super();
	}

	protected List<Help> getHelperMethods(Method[] methods) {
		List<Help> list = new ArrayList<>();
		Arrays.stream(methods)
			.filter(a -> a.isAnnotationPresent(ResponseBody.class) && !a.getName().equals("help"))
			.forEach(a -> list.add(new Help(a.getName(), getParameterNames(a))));
		return list;
	}

	private List<String> getParameterNames(Method a) {
		return Arrays.stream(a.getParameters())
				.filter(p -> p.isAnnotationPresent(RequestParam.class))
				.map(p -> p.getAnnotation(RequestParam.class).value())
				.collect(Collectors.toList());
	}

}