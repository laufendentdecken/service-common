package at.running.platform.common.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Help {
	private String arg;
	private List<String> params;
}
